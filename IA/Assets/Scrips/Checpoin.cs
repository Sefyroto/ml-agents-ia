﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checpoin : MonoBehaviour
{
    public Entrenamiento coche;
    public metas meta;

    // Start is called before the first frame update
    void Awake()
    {
        coche.respawnear += RespawnChecpoins;
        meta.respawnear += RespawnChecpoins;
    }

    public void RespawnChecpoins()
    {
        foreach(Transform hijos in transform)
        {
            if (!hijos.gameObject.activeSelf)
            {
                hijos.gameObject.SetActive(true);
            }
        }
    }

}
