﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metas : MonoBehaviour
{
    public GameObject tope;
    public Entrenamiento coche;

    public delegate void Respawn();
    public event Respawn respawnear;

    // Start is called before the first frame update
    void Awake()
    {
        coche.llegarMeta += llegarAMeta;
    }

    public void llegarAMeta()
    {
        respawnear.Invoke();
        tope.SetActive(false);
    }

}
