﻿using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class Entrenamiento : Agent
{
    public Rigidbody rb;
    public int velocidad;
    public int rotarRuedas;
    Vector3 posicionInicial;
    Vector3 rotacionInicial;
    public delegate void Respawn();
    public event Respawn respawnear;
    public delegate void meta();
    public event meta llegarMeta;
    float conteoChecpoint=0;
    public Transform checpoints;

    public override void Initialize()
    {
        //cada vez que se inicie podra de base la rotacion y la posicion de el choche
        rotacionInicial = this.transform.localEulerAngles;
        posicionInicial = this.transform.localPosition;
        //print(rotacionInicial + " " + posicionInicial);
    }

    public override void OnEpisodeBegin()
    {
        //cada vez que empieze otro episodio retornara a la base la rotacion y la posicion de el choche
        this.transform.localPosition = posicionInicial;
        this.transform.localEulerAngles = rotacionInicial;
        conteoChecpoint = 0;//los checpoints pasan a ser 0
        respawnear.Invoke();//llama a todos los que esten suscritos a el evento "es un delegado"
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        /*foreach (Transform hijos in transform)
        {
            sensor.AddObservation(hijos);
        }*/
        sensor.AddObservation(this.transform.localPosition);
    }
    public override void OnActionReceived(ActionBuffers actions)
    {
        float movimientoX = actions.ContinuousActions[0]; //es la accion de girar a izquierda y derecha
        float movimientoZ = actions.ContinuousActions[1]; //es la accion de acelerar o marcha atras
         
        this.transform.localPosition += (transform.forward * movimientoZ * velocidad * Time.deltaTime);
   
        this.transform.Rotate(new Vector3(0, movimientoX, 0));
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        ActionSegment<float> valorAccion = actionsOut.ContinuousActions;
        valorAccion[0] = Input.GetAxis("Horizontal");
        valorAccion[1] = Input.GetAxis("Vertical");

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "meta") {
            print("Meta");
            AddReward(20f);
            llegarMeta.Invoke();
        } 
        else if (other.tag == "checkpoint") 
        {
            other.gameObject.SetActive(false);
            print("checkpoint");
            conteoChecpoint += 0.2f;
            AddReward(1f+conteoChecpoint);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "pared")
        {
            AddReward(-0.75f);
            EndEpisode();
        }
    }
}
